package es.ivanamat.secure.secureauth;

import java.security.Principal;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import es.ivanamat.secure.secureauth.model.Role;
import es.ivanamat.secure.secureauth.model.User;
import es.ivanamat.secure.secureauth.repository.UserRepository;
import es.ivanamat.secure.secureauth.service.CustomUserDetailsServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.AuthorizationScope;

@Api(tags="Main", description="Main controller")
@RestController
@RequestMapping("/")
public class MainResource {

    @Value("${spring.application.name}")
    private String appName;
    
    @Value("${spring.application.version}")
    private String appVersion;   

	@Autowired
	private UserRepository userRepository;

    @ApiOperation(value = "Home", nickname = "home", notes = "", response = String.class, tags={ "Main", })
	@GetMapping
	public String index() {
		return appName+":v"+appVersion;
	}

    @ApiOperation(value = "Get user logged data", nickname = "getPrincipal", notes = "", response = Principal.class, authorizations = {
            @Authorization(value = "token_auth", scopes = {
                @AuthorizationScope(scope = "read:principal", description = "read your data")
                })
        }, tags={ "Main", })
        @ApiResponses(value = { 
            @ApiResponse(code = 200, message = "An object of the logged user", response = Principal.class),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 405, message = "Invalid input") })
	@GetMapping("/principal")
	public Principal user(Principal principal) {
		return principal;
	}
	
    @ApiOperation(value = "Logout user", nickname = "logoutUser", notes = "", response = String.class, tags={ "Main", })
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return "redirect:/?logout";
	}
	
	@PostConstruct
	public void postConstruct() {
		
		Set<Role> roles = new HashSet<Role>();
		Role r = new Role("ROLE_USER");
		roles.add(r);
		
		CustomUserDetailsServiceImpl userService = new CustomUserDetailsServiceImpl(userRepository);
		User u = new User();
		u.setActive(1);
		u.setUsername("ivan");
		u.setPassword("$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu");
		u.setDomain(null);
		u.setName("Iván");
		u.setLastName("Amat");
		u.setEmail("dev@ivanamat.es");
		u.setRoles(roles);
		u.setAccountNonExpired(true);
		u.setAccountNonLocked(true);
		u.setCredentialsNonExpired(true);
		u.setEnabled(true);
		
		userService.save(u);
	}
}