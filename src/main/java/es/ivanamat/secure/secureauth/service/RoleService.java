package es.ivanamat.secure.secureauth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.ivanamat.secure.secureauth.model.Role;
import es.ivanamat.secure.secureauth.repository.RoleRepository;

@Service
public class RoleService {
	
	@Autowired
	private RoleRepository roleRepository;

	public RoleService(RoleRepository roleRepository) {
		this.roleRepository = roleRepository;
	}

	public Role findById(Integer id){
		return roleRepository.findById(id).get();
	}
	
	public Role save(Role role) {
		return this.roleRepository.save(role);
	}

}
