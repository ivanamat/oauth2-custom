package es.ivanamat.secure.secureauth.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import es.ivanamat.secure.secureauth.model.User;
import es.ivanamat.secure.secureauth.repository.UserRepository;

@Service("userDetailsService")
public class CustomUserDetailsServiceImpl implements CustomUserDetailsService {
	
	private String domain;

	@Autowired
    private UserRepository userRepository;
    
    public CustomUserDetailsServiceImpl() {
    }
	
    public CustomUserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsernameAndDomain(String username, String domain) throws UsernameNotFoundException {
        if (this.isAnyBlank(username, domain)) {
            throw new UsernameNotFoundException("Username and domain must be provided");
        }
        
        this.domain = domain;
        
        UserDetails user = userRepository.findUserByUsernameAndDomain(username, domain);
        if (user == null) {
            throw new UsernameNotFoundException(
                String.format("Username not found for domain, username=%s, domain=%s", username, domain));
        }
        return user;
    }

    public User save(User user) {
    	return userRepository.save(user);
    }
    
    private boolean isAnyBlank(String... strings) {
    	for(String string : strings) {
    		if(string.isEmpty() || string == null) {
    			return true;
    		}
    	}
    	
    	return false;
    }

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (this.isAnyBlank(username)) {
            throw new UsernameNotFoundException("Username must be provided");
        }
        UserDetails user = userRepository.findUserByUsername(username);
        if (user == null || domain != null && domain != "") {
            throw new UsernameNotFoundException(
                String.format("Username not found for domain, username=%s", this.domain));
        }
        return user;
	}
	
}
