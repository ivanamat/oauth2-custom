package es.ivanamat.secure.secureauth.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface CustomUserDetailsService extends UserDetailsService {

//	UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;
    UserDetails loadUserByUsernameAndDomain(String username, String domain) throws UsernameNotFoundException;
}