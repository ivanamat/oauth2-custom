package es.ivanamat.secure.secureauth.config;

import static springfox.documentation.builders.PathSelectors.regex;

import java.io.IOException;
import java.net.URISyntaxException;
import java.time.temporal.Temporal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.common.base.Predicate;

import io.swagger.models.auth.In;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.OAuthBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationCodeGrant;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.GrantType;
import springfox.documentation.service.OAuth;
import springfox.documentation.service.ResourceOwnerPasswordCredentialsGrant;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.service.TokenEndpoint;
import springfox.documentation.service.TokenRequestEndpoint;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.ApiKeyVehicle;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
    @Value("${spring.application.name}")
    private String applicationName;
    @Value("${spring.application.description}")
    private String applicationDescription;
    @Value("${spring.application.version}")
    private String applicationVersion;
    @Value("${app.client.id}")
    private String clientId;
    @Value("${app.client.secret}")
    private String clientSecret;

    @Value("${host.full.dns.auth.link}")
    private String baseUri;

    @Bean
    public Docket api() throws IOException, URISyntaxException {
    	final List<ResponseMessage> globalResponses = Arrays.asList(
    	        new ResponseMessageBuilder()
    	            .code(200)
    	            .message("OK")
    	            .build(),
    	        new ResponseMessageBuilder()
    	            .code(400)
    	            .message("Bad Request")
    	            .build(),
    	        new ResponseMessageBuilder()
    	            .code(500)
    	            .message("Internal Error")
    	            .build());
    	
        return new Docket(DocumentationType.SWAGGER_2)
        		.select()
        		.apis(RequestHandlerSelectors.basePackage("es.ivanamat.secure.secureauth"))
                .paths(PathSelectors.any()).build().securitySchemes(Collections.singletonList(securitySchema()))
                .securityContexts(Collections.singletonList(securityContext())).pathMapping("/")
                .useDefaultResponseMessages(false).apiInfo(apiInfo())
    	        .globalResponseMessage(RequestMethod.GET, globalResponses)
    	        .globalResponseMessage(RequestMethod.POST, globalResponses)
    	        .globalResponseMessage(RequestMethod.PUT, globalResponses)
    	        .globalResponseMessage(RequestMethod.PATCH, globalResponses)
    	        .globalResponseMessage(RequestMethod.DELETE, globalResponses);
    }
	
	private OAuth securitySchema() {

		List<AuthorizationScope> authorizationScopeList = new ArrayList<AuthorizationScope>();
		authorizationScopeList.add(new AuthorizationScope("read", "read all"));
		authorizationScopeList.add(new AuthorizationScope("trust", "trust all"));
		authorizationScopeList.add(new AuthorizationScope("write", "access all"));
		authorizationScopeList.add(new AuthorizationScope("ROLE_USER", "access as user"));

		List<GrantType> grantTypes = new ArrayList<GrantType>();
		GrantType creGrant = new ResourceOwnerPasswordCredentialsGrant(baseUri + "/oauth/token?domain=");

		grantTypes.add(creGrant);

		return new OAuth("oauth2schema", authorizationScopeList, grantTypes);

	}
	
    private SecurityContext securityContext() {
        return SecurityContext.builder()
        		.securityReferences(defaultAuth())
        		.forPaths(PathSelectors.ant("/**")).build();
    }
	
    private List<SecurityReference> defaultAuth() {

        final AuthorizationScope[] authorizationScopes = new AuthorizationScope[3];
        authorizationScopes[0] = new AuthorizationScope("read", "read all");
        authorizationScopes[1] = new AuthorizationScope("trust", "trust all");
        authorizationScopes[2] = new AuthorizationScope("write", "write all");
        // oauth2schema
        return Collections.singletonList(new SecurityReference("OAuth2", authorizationScopes));
    }

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("OAuth2 Authentication API").description(
				"This is a sample Auth2 authentication service. For this sample, you can use the `admin` or `client` users (password: admin and client respectively) to test the authorization filters. Once you have successfully logged in and obtained the token, you should click on the right top button `Authorize` and introduce it with the prefix \"Bearer \".")
				.version("1.0.0").license("MIT License").licenseUrl("http://opensource.org/licenses/MIT")
				.contact(new Contact("Iván Amat", "https://www.ivanamat.es/", "dev@ivanamat.es")).build();
	}

}
