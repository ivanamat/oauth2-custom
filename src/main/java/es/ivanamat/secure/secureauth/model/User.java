package es.ivanamat.secure.secureauth.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "users")
@ApiModel(description="Modelo de usuario")
public class User implements UserDetails {

	private static final long serialVersionUID = 1L;
    
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
	@ApiModelProperty(notes = "Unique identifier of the user. No two persons can have the same id.", example = "1", required = true, position = 0)
    private int id;

    @Column(name = "email")
    private String email;

    @Column(name = "username", nullable = false)
    private String username;
    @Column(name = "password", nullable = false)
    private String password;
    @Column(name = "domain", nullable = true)
    private String domain = null;
    @Column(name = "name")
    private String name;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "active")
    private int active;
    @Column(name = "account_non_expired")
	private boolean accountNonExpired;
    @Column(name = "account_non_locked")
	private boolean accountNonLocked;
    @Column(name = "credentials_non_expired")
	private boolean credentialsNonExpired;
    @Column(name = "enabled")
	private boolean enabled;

//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//    @JoinTable(name = "user_role", joinColumns =
//    @JoinColumn(name = "user_id"), inverseJoinColumns =
//    @JoinColumn(name = "role_id"))
//    private Set<Role> roles;
    
	@JsonIgnore
	@ManyToMany(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
 	private Set<Role> roles = new HashSet<Role>();	 	

    public User() {
    }

    public User(User user) {
    	this.active = user.active;
    	this.username = user.username;
    	this.password = user.password;
    	this.domain = user.domain;
        this.name = user.name;
        this.lastName = user.lastName;
        this.email = user.email;        
        this.roles = user.roles;
    	this.accountNonExpired = user.accountNonExpired;
        this.accountNonLocked = user.accountNonLocked;
        this.credentialsNonExpired = user.credentialsNonExpired;
        this.enabled = user.enabled;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isAccountNonExpired() {
		return accountNonExpired;
	}
	
	public void setAccountNonExpired(boolean accountNonExpired) {
		this.accountNonExpired = accountNonExpired;
	}

	@Override
	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}
	
	public void setAccountNonLocked(boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}
	
	public void setCredentialsNonExpired(boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}
	
	@Override
	public boolean isEnabled() {
		return enabled;
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

//	public String toString() {
//		StringBuilder sb = new StringBuilder();
//		sb.append(super.toString()).append(": ");
//		sb.append("Username: ").append(this.username).append("; ");
//		sb.append("Password: [PROTECTED]; ");
//		sb.append("Enabled: ").append(this.enabled).append("; ");
//		sb.append("AccountNonExpired: ").append(this.accountNonExpired).append("; ");
//		sb.append("credentialsNonExpired: ").append(this.credentialsNonExpired)
//				.append("; ");
//		sb.append("AccountNonLocked: ").append(this.accountNonLocked).append("; ");
//
////		if (!authorities.isEmpty()) {
////			sb.append("Granted Authorities: ");
////
////			boolean first = true;
////			for (GrantedAuthority auth : authorities) {
////				if (!first) {
////					sb.append(",");
////				}
////				first = false;
////
////				sb.append(auth);
////			}
////		}
////		else {
////			sb.append("Not granted any authorities");
////		}
//
//		return sb.toString();
//	}

}