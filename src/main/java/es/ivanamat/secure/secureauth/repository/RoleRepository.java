package es.ivanamat.secure.secureauth.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import es.ivanamat.secure.secureauth.model.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Optional<Role> findByRole(String role);
}
