package es.ivanamat.secure.secureauth.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.userdetails.UserDetails;

import es.ivanamat.secure.secureauth.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {
	
	public static final String FIND_PROJECTS = "SELECT * FROM users WHERE username=:#{#username} and domain IS NULL;";

	User findUserByUsernameAndDomain(String username, String domain);
	
	@Query(value = FIND_PROJECTS, nativeQuery = true)
	User findUserByUsername(@Param("username") String username);
//	UserDetails findOneByUsername(String username);
//	UserDetails findOneByUsernameAndPassword(String username, String password);

//	public default User findUser(String username) {
//		return findUserByUsername(username);
//	}
	
//	public default User findUser(String username, String domain) {
//		return findUserByUsernameAndDomain(username,domain);
//	}
	
}
